﻿using CM.API.ResponseModels;
using CM.Core.Dtos;
using CM.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ApiController
    {
        private readonly IClientAppService _clientAppService;

        public ClientController(IClientAppService clientAppService)
        {
            _clientAppService = clientAppService;
        }

        [AllowAnonymous]
        [HttpGet("List")]
        public async Task<IEnumerable<ClientInformationDto>> GetClientsAsync()
        {
            return await _clientAppService.GetAll();
        }

        [AllowAnonymous]
        [HttpGet("Get/{id:int}")]
        public async Task<ClientInformationDto> GetClientAsync(int id)
        {
            return await _clientAppService.GetById(id);
        }

        [AllowAnonymous]
        [HttpPost("Create")]
        public async Task<IActionResult> PostClientAsync([FromBody] ClientInformationDto clientDto)
        {
            return !ModelState.IsValid ? CustomResponse(ModelState) : CustomResponse(await _clientAppService.Add(clientDto));
        }

        [AllowAnonymous]
        [HttpPut("Update")]
        public async Task<IActionResult> PutClientAsync([FromBody] ClientInformationDto clientDto)
        {
            return !ModelState.IsValid ? CustomResponse(ModelState) : CustomResponse(await _clientAppService.Update(clientDto));
        }

        [AllowAnonymous]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> DeleteClientAsync(int id)
        {
            return CustomResponse(await _clientAppService.Remove(id));
        }
    }
}