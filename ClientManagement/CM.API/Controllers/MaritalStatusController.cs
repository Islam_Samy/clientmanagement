﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CM.Core.Dtos;
using CM.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaritalStatusController : ApiController
    {

        #region Ctors+Fileds
        private readonly IMaritalStatusService _maritalStatusService;
        public MaritalStatusController(IMaritalStatusService maritalStatusService)
        {
            _maritalStatusService = maritalStatusService;
        }

        #endregion


        [AllowAnonymous]
        [HttpGet("List")]
        public async Task<IEnumerable<MaritalStatusDto>> GetMaritalStatusesAsync()
        {
            return await _maritalStatusService.ListStatues();
        }
    }
}