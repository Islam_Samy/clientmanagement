﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CM.API.ResponseModels
{
    public class ResponseModel : IResponseModel
    {
        public List<string> Errors { get; set; }
        public dynamic Data { get; set; }
        public int Status { get; set; }

        public IResponseModel Response(List<string> errors, dynamic data, int status)
        {
            this.Errors = errors;
            this.Data = data;
            this.Status = status;
            return this;
        }
    }
}
