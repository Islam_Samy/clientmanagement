﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CM.API.ResponseModels
{
    public interface IResponseModel
    {
        IResponseModel Response(List<string> errors, dynamic data, int status);
    }
}
