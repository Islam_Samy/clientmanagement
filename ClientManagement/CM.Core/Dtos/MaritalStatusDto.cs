﻿using System.Collections.Generic;

namespace CM.Core.Dtos
{
    public class MaritalStatusDto
    {
        public int MaritalStatusId { get; set; }
        public string StatusText { get; set; }
    }
}