﻿using AutoMapper;
using CM.Core.Dtos;
using CM.Core.Interfaces;
using CM.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CM.Core.Services
{
    public class MaritalStatusService : IMaritalStatusService
    {

        #region Ctos+Fileds
        private readonly IMaritalStatusRepository _maritalStatusRepo;
        private readonly IMapper _mapper;

        public MaritalStatusService(IMaritalStatusRepository maritalStatusRepo, IMapper mapper)
        {
            _mapper = mapper;
            _maritalStatusRepo = maritalStatusRepo;
        } 
        #endregion

        public async Task<IEnumerable<MaritalStatusDto>> ListStatues()
        {
            return _mapper.Map<IEnumerable<MaritalStatusDto>>(await _maritalStatusRepo.GetAll());
        }
    }
}
