﻿using AutoMapper;
using CM.Core.Dtos;
using CM.Core.Interfaces;
using CM.DAL.Models;
using CM.DAL.Repositories;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CM.Core.Services
{
    public class ClientAppService : IClientAppService
    {
        private readonly IMapper _mapper;
        private readonly IClientRepository _clientRepo;

        public ClientAppService(IMapper mapper, IClientRepository clientRepo)
        {
            _mapper = mapper;
            _clientRepo = clientRepo;
        }

        public async Task<int> Add(ClientInformationDto client)
        {
            if (client == null)
                return 0;

            return await _clientRepo.Add(_mapper.Map<ClientInformation>(client));
        }

        public async Task<IEnumerable<ClientInformationDto>> GetAll()
        {
            return _mapper.Map<IEnumerable<ClientInformationDto>>(await _clientRepo.GetAll());
        }

        public async Task<ClientInformationDto> GetById(int id)
        {
            return _mapper.Map<ClientInformationDto>(await _clientRepo.GetById(id));
        }

        public async Task<int> Remove(int clientId)
        {
            var client = await _clientRepo.GetById(clientId);

            if (client != null)
                return await _clientRepo.Remove(_mapper.Map<ClientInformation>(client));

            return 0;
        }

        public async Task<int> Update(ClientInformationDto client)
        {
            if (client == null || !_clientRepo.ClientExists(client.ClientId))
                return 0;

            return await _clientRepo.Update(_mapper.Map<ClientInformation>(client));
        }
    }
}