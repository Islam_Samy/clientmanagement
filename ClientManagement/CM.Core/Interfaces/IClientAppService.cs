﻿using CM.Core.Dtos;
using CM.DAL.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CM.Core.Interfaces
{
    public interface IClientAppService
    {
        Task<ClientInformationDto> GetById(int id);
        Task<IEnumerable<ClientInformationDto>> GetAll();
        Task<int> Add(ClientInformationDto client);
        Task<int> Update(ClientInformationDto client);
        Task<int> Remove(int clientId);
    }
}
