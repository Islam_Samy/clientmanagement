﻿using CM.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CM.Core.Interfaces
{
    public interface IMaritalStatusService
    {
        Task<IEnumerable<MaritalStatusDto>> ListStatues();
    }
}
