﻿using AutoMapper;
using CM.Core.Dtos;
using CM.DAL.Models;

namespace CM.Core.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Mapping  Entity - DTO
            CreateMap<ClientInformation, ClientInformationDto>().ReverseMap();
            CreateMap<MaritalStatus, MaritalStatusDto>().ReverseMap();

        }
    }
}