﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CM.UI.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CM.UI.Web.Controllers
{
    public class ClientManagement : Controller
    {
        private readonly IClientRESTService _clientRESTService;
        public  string _apiUrl { get; private set; }
        public ClientManagement(IClientRESTService clientRESTService, IConfiguration configuration)
        {
            _clientRESTService = clientRESTService;
            _apiUrl = configuration.GetSection("ApiUrl").Value;
        }

    }
}
