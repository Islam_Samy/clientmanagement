﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CM.UI.Web.Services
{
    public interface IClientRESTService
    {
        Task<HttpResponseMessage> ConsumeClientRESTService(string baseurl, HttpMethod method, string relativeUrl, HttpContent content = null,string jsonObj = null);
    }
}
