﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;

namespace CM.UI.Web.Services
{
    public class ClientRESTService : IClientRESTService
    {
        #region Ctors+Fields
        private readonly IConfiguration _configuration;
        public ClientRESTService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        #endregion

        public async Task<HttpResponseMessage> ConsumeClientRESTService(string baseurl, HttpMethod method, string relativeUrl, HttpContent content = null, string jsonObj = null)
        {
            using (var client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(method, relativeUrl);
                if (jsonObj != null)
                {
                    request.Content = new StringContent(jsonObj, Encoding.UTF8, "application/json");
                }
                if (content != null)
                {
                    request.Content = content;
                }
                var Res = new HttpResponseMessage();
                try
                {
                    return await client.SendAsync(request);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }


    }
}
