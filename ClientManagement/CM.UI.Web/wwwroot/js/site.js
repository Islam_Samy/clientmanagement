﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

///List
listClientsAjax();
function drawTable(data) {
    console.log(data);
    var rows = ``;
    $.each(data, function (key, value) {
        rows += "<tr>" +
            "<td  style='display: none;' value=" + value.clientId + ">" + value.clientId + "</td>" +
            "<td>" + value.firstName + "</td>" +
            "<td>" + value.lastName + "</td>" +
            "<td>" + new Date(value.dob).toLocaleDateString() + "</td>" +
            "<td>" + value.maritalStatusId + "</td>" +
            "<td>" + value.mobileNumber + "</td>" +
            "<td>" + value.email + "</td>" +
            "<td>" +
            "<button type='button' onclick='editClient(" + value.clientId + ");' class='btn btn-default'>" +
            "<i class='fas fa-edit fa-sm' style='color: blue;'></i>" +
            "</button>" +
            "</td>" +
            "<td>" +
            "<button id='btn-delete' type='button' onclick='deleteClient(this," + value.clientId + ");' class='btn btn-default'>" +
            "<i class='fas fa-trash fa-xs' style='color: red;'></i>" +
            "</button>" +
            "</td>" +
            "</tr>";
    });

    var clientsTable = `<thead><tr><th>First Name</th><th>Last Name</th><th>Birth Date</th><th>Marital Status</th><th>Mobile Number</th><th>Email</th><th></th><th></th></tr></thead><tbody>${rows}</tbody>`;

    if (data.length != 0) {
        $('#clientsTable').append(clientsTable);
    }
};
async function listClientsAjax() {
    $.get("https://localhost:8024/api/Client/List", function (data) {
        drawTable(data);
    });
};

///Delete
async function deleteClient(ele, clientId) {
    $.ajax({ url: "https://localhost:8024/api/Client/Delete/" + clientId + "", method: "DELETE" })
        .then(function (data) {
            $(ele).closest('tr').fadeOut(500);
            setTimeout(function () {
                $(ele).closest('tr').remove()
            }, 1000);

            toastr.success('client deleted sucessfully');
        })
        .catch(function (err) {
            console.log('ERROR');
            //some code...
        });
};


///Fill dropdown List
$.get('https://localhost:8024/api/MaritalStatus/List', function (response) {
    $.each(response, function (key, value) {
        let option = new Option(value.statusText, value.maritalStatusId);
        $(option).html(value.statusText);
        $("#maritalstatus").append(option);
    });
});


///open create Form
function OpenForm(id) {
    //jquery datetime picker
    $('#birthdate').datetimepicker({
        dateFormat: "yy-mm-dd",
        timepicker: false,
        lang: 'en',
        maxDate: new Date(),
        onSelect: function () {
            $(this).blur().change();
        }

    });

    //select2
    $("#maritalstatus").select2({
        width: '100%',
        placeholder: "select marital status..",
        allowClear: true
    });

    //modal show form
    let modalForm = $("#addClientModal");
    modalForm.modal("show");
}


///search 
$('#search').on("keyup", function () {
    var value = $(this).val().toLocaleLowerCase();
    $('tbody tr').filter(function () {
        $(this).toggle($(this).text().toLocaleLowerCase().indexOf(value) > -1);
    });
});


///Create client
async function saveClient() {
    console.log('inside save');
    let clientForm = $('#clientForm');
    if (false) {
    }
    else {

        let formDataArray = clientForm.serializeArray();
        var data = new FormData();
        for (var i = 0; i < formDataArray.length; i++) {
            data.append(formDataArray[i].name, formDataArray[i].value);
        }
        var object = {};
        data.forEach(function (value, key) {
            object[key] = value;
        });
        var jsonObj = JSON.stringify(object);

        console.log(jsonObj);

        $.ajax({
            url: "http://localhost:8031/api/Client/Create",
            type: "POST",
            data: function () {
                return jsonObj;
            }(),
            contentType: false,
            processData: false,
            success: function (response) {
                let modalForm = $("#addClientModal");
                if (true) {
                    console.log(response);
                    modalForm.modal("hide");
                    toastr.success('saved');
                }
                else {
                    toastr.error('ERROR');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                toastr.error('ERROR please try again')
                console.log(errorMessage);
            }
        });
    }
}
