﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CM.DAL.Models
{
    public partial class ClientManagementContext : DbContext
    {
        public ClientManagementContext()
        {
        }

        public ClientManagementContext(DbContextOptions<ClientManagementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClientInformation> ClientInformation { get; set; }
        public virtual DbSet<MaritalStatus> MaritalStatus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=ISLAM\\ISLAM;Database=ClientManagement;User Id=sql;Password=Sql@12345;Trusted_Connection=false;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientInformation>(entity =>
            {
                entity.HasKey(e => e.ClientId);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.MobileNumber).HasMaxLength(50);

                entity.HasOne(d => d.MaritalStatus)
                    .WithMany(p => p.ClientInformation)
                    .HasForeignKey(d => d.MaritalStatusId)
                    .HasConstraintName("FK_ClientInformation_MaritalStatus");
            });

            modelBuilder.Entity<MaritalStatus>(entity =>
            {
                entity.Property(e => e.StatusText).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
