﻿using System.Collections.Generic;

namespace CM.DAL.Models
{
    public partial class MaritalStatus
    {
        public MaritalStatus()
        {
            ClientInformation = new HashSet<ClientInformation>();
        }

        public int MaritalStatusId { get; set; }
        public string StatusText { get; set; }

        public virtual ICollection<ClientInformation> ClientInformation { get; set; }
    }
}