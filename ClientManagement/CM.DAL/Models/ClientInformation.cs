﻿using System;

namespace CM.DAL.Models
{
    public partial class ClientInformation
    {
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Dob { get; set; }
        public int? MaritalStatusId { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }

        public virtual MaritalStatus MaritalStatus { get; set; }
    }
}