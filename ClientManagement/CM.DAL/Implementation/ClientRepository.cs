﻿using CM.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;
using CM.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq;

namespace CM.DAL.Implementation
{
    public class ClientRepository : IClientRepository
    {
        #region Ctors+Fileds
        private readonly ClientManagementContext Db;
        private readonly DbSet<ClientInformation> DbSet;

        public ClientRepository(ClientManagementContext context)
        {
            Db = context;
            DbSet = Db.Set<ClientInformation>();
        }
        #endregion

        public async Task<int> Add(ClientInformation clientInfo)
        {
            DbSet.Add(clientInfo);
            return await Db.SaveChangesAsync();
        }

        public async Task<IEnumerable<ClientInformation>> GetAll()
        {
            return await DbSet.ToListAsync();
        }

        public async Task<ClientInformation> GetById(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task<int> Remove(ClientInformation client)
        {
            DbSet.Remove(client);
            return await Db.SaveChangesAsync();
        }

        public async Task<int> Update(ClientInformation client)
        {
            try
            {
                DbSet.Update(client);
                return await Db.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException ex)
            {
                return 0;
            }
        }

        public bool ClientExists(int id)
        {
            return DbSet.Any(e => e.ClientId == id);
        }
    }
}
