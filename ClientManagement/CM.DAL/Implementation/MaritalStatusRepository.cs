﻿using CM.DAL.Models;
using CM.DAL.Repositories;
using Dapper.Contrib.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace CM.DAL.Implementation
{
    public class MaritalStatusRepository : IMaritalStatusRepository
    {
        #region Ctors+Fileds
        private readonly ClientManagementContext Db;
        private readonly DbSet<MaritalStatus> DbSet;

        public MaritalStatusRepository(ClientManagementContext context)
        {
            Db = context;
            DbSet = Db.Set<MaritalStatus>();
        }
        #endregion

        public async Task<IEnumerable<MaritalStatus>> GetAll()
        {
            return await DbSet.ToListAsync();
        }
    }
}
