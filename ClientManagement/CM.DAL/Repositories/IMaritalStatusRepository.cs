﻿using CM.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CM.DAL.Repositories
{
    public interface IMaritalStatusRepository
    {
        Task<IEnumerable<MaritalStatus>> GetAll();

    }
}
