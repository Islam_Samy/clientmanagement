﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CM.DAL.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CM.DAL.Repositories
{
    public interface IClientRepository
    {
        Task<ClientInformation> GetById(int clientId);
        Task<IEnumerable<ClientInformation>> GetAll();
        Task<int> Add(ClientInformation client);
        Task<int> Update(ClientInformation client);
        Task<int> Remove(ClientInformation client);
        bool ClientExists(int clientId);


    }
}
